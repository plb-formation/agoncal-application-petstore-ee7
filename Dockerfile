FROM jetty:9.4.14-jre8

# Add target directory war file coming from a jenkins build  in tomcat
ADD target/applicationPetstore*.war /var/lib/jetty/webapps/
